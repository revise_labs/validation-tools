# Validation Tools 3.1.0 (Groovy)

Validation tools is a simple drop-in library that aids with Bean validation on the JVM.

One can say that it's a thin wrapper around the Hibernate Validator that simply provides convenience methods for the usual validation tasks, plus a little extra. With that
in mind, anything you can do using Hibernate Validator, you can do with RL Validation Tools with minimal effort.

## Installation

Add the `validation-tools` library to your project:

```
dependencies {
    compile group: 'com.reviselabs', name: 'validation-tools', version: '3.1.0'
}
```

OR

```
include 'validation-tools'

project(':validation-tools').projectDir = file('../validation-tools')

dependencies {
  compile project(':validation-tools')
}
```

## Usage

### Initialization

You start by creating a Bean that possesses any of the Hibernate constraint annotations. For example:

```
class UserForm {
    @Required @Email private String email; //Email is required and must be a valid email address
    @Required @Min(8) private String password; //Password is required and must be at least 8 characters long

    /* Getters and setters */
}
```

To create a form based on this Bean, simply do:

```
Form<UserForm> signupForm = Form.form(UserForm.class)
```

You can then "populate" this form from a variety of common data types and collection sets via the `bind()` method.
This method has several signatures that accept the following types:

* An `Object`
* A `Map<String, Object>`
* A well-formed JSON `String`

For example:

```
Map<String, Object> params = new HashMap<>();
params.put("email", "me@mail.com");
params.put("password", "password01");
signupForm.bind(params) //Fills the form with values for email and password.
```

### Validation

After populating the form via whatever means you choose, you can call `valid()` to run validation checks and verify the form's
validity. If the form isn't valid, you can retrieve the errors via `getErrors()` which will return a `java.util.ArrayList` of
`com.reviselabs.validation.Error` objects or `getErrorsAsJson()` to return a JSON string representation of the form errors.

```
if(signupForm.valid()) { /* Continue on */ }
else { response.renderJSON(signupForm.getErrorsAsJson()); }
```

### Retrieve the underlying Bean

After you've performed your validation, you can retrieve the underlying Bean for further processing. The Bean will
already be populated with the form's values.

```
UserForm accountData = signupForm.get();
database.createAccount(accountData);
```

## Other Uses

### Map a form to another POJO

Using the above form an an example, you can copy its properties (`email` and `password`) to another POJO that has the same properties. E.g.

```
User user = database.findUserById(1); // Class User has String email
UserForm filledForm = Form.form(UserForm.class).bind(request.params());
if(filledForm.valid()) {
    filledForm.copyTo(user); //Copy properties from the form the User instance.
    database.update(user)
}
```

The target Bean can be of any type, as long as the field names and types are compatible. The target bean does not have to have
all of the same properties of the form.




