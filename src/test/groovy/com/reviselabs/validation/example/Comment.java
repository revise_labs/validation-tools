package com.reviselabs.validation.example;

/**
 * Created by ksheppard on 09/11/2016.
 */
public class Comment {
    private long id;
    private String text;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Comment() {}

    public Comment(long id, String text) {
        this.id = id;
        this.text = text;
    }
}
