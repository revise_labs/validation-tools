package com.reviselabs.validation.example;

import com.reviselabs.validation.constraints.Required;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class PostForm {
    private long id;
    @Required private String title;
    @Required private String body;
    @NotNull(message = "You must provide an author")
    private Author author;
    private List<Comment> comments;

    {
        id = 2015;

        author = new Author();
        author.setId(10);
        author.setName("Venkat");

        comments = new ArrayList<Comment>();
        comments.add(new Comment(100, "Ha!"));
        comments.add(new Comment(101, "yo!"));

        title = "A great post";
        body = "This great post too plenty of time to write.";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
