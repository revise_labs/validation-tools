import com.reviselabs.validation.Form
import com.reviselabs.validation.example.Post
import com.reviselabs.validation.example.PostForm
import groovy.json.JsonOutput

class FormsTest extends GroovyTestCase {

  void testGetAs() {
      Form<PostForm> filledForm = Form.form(PostForm).bind(new PostForm());
      def post = filledForm.getAs(Post);
      assertNotNull(post);
      assertFalse(post.comments.empty);
  }

  void testGetSubObject() {
    Form<PostForm> filledForm = Form.form(PostForm).bind(new PostForm());
    def postForm = filledForm.get();
    assertNotNull(postForm)
    assertEquals(postForm.author.name, "Venkat");
  }

  void testBindFromObject() {
    Form<PostForm> filledForm = Form.form(PostForm).bind(new Post(title: "A new post", body: "Fresh page"));
    def postForm = filledForm.get()
    assertNotNull(postForm)
    assertEquals(postForm.title, "A new post");
    assertEquals(postForm.body, "Fresh page");
  }

  void testBindFromMap() {
    Form<PostForm> filledForm = Form.form(PostForm).bind([id: 100, title: "A new post", body: "Fresh page"]);
    def postForm = filledForm.get()
    assertNotNull(postForm)
    assertEquals(postForm.id, 100);
    assertEquals(postForm.title, "A new post");
    assertEquals(postForm.body, "Fresh page");
  }

  void testBindFromJson() {
    def params = [title: "A new post", body: "Fresh page", id: 100, author: [ id: 60, name: 'Tom Clancy'] ]
    Form<PostForm> filledForm = Form.form(PostForm).bind(JsonOutput.toJson(params));
    def postForm = filledForm.get()
    assertNotNull(postForm)
    assertEquals(postForm.id, 100)
    assertEquals(postForm.title, "A new post")
    assertEquals(postForm.body, "Fresh page")
    assertEquals(postForm.author.name, "Tom Clancy")
  }

  void testCopyTo() {
    def post = new Post()
    def frmPost = Form.form(PostForm) // Not filling it here since PostForm constructor sets fields.
    frmPost.copyTo(post)
    assertNotNull(post.title)
    assertEquals(post.title, frmPost.get().title)
  }
}
