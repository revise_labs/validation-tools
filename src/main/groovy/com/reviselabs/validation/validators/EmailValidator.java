package com.reviselabs.validation.validators;

import com.reviselabs.validation.constraints.Email;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * Created by ksheppard on 03/08/2016.
 */
public class EmailValidator implements ConstraintValidator<Email, String> {
    @Override
    public void initialize(Email email) {  }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if(value != null) {
            Pattern EMAIL_PATTERN = Pattern.compile("^[a-zA-Z][\\w\\d\\-\\.\\$\\+]+@[a-zA-Z0-9-]{2,}+\\.(?:[a-zA-Z0-9\\.\\-])*[a-z]{2,}$");
            return EMAIL_PATTERN.matcher(value).matches();
        } else return true;
    }
}
