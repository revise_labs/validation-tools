package com.reviselabs.validation.validators;

import com.reviselabs.validation.constraints.Required;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by ksheppard on 03/08/2016.
 */
public class RequiredValidator implements ConstraintValidator<Required, Object> {
    @Override
    public void initialize(Required required) {}

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        return value != null && !(String.class.isInstance(value) && value.toString().isEmpty());
    }
}
